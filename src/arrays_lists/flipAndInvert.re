let flipAndInvert = (rowsMatrix: list(list(int))) =>
  List.map(
    currentRow =>
      List.fold_left(
        (reversedList, num) => [num == 1 ? 0 : 1, ...reversedList],
        [],
        currentRow
      ),
    rowsMatrix
  );

let testMatrix = [
  [0, 0, 0, 0, 1, 1, 1, 1],
  [0, 0, 0, 0, 1, 1, 1, 1],
  [0, 0, 0, 0, 1, 1, 1, 1],
  [0, 0, 0, 0, 1, 1, 1, 1]
];

testMatrix |> flipAndInvert |> Js.log;