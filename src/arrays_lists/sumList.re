let concatList = stringList =>
  List.fold_left(
    (concatString, currentString) => concatString ++ currentString,
    "",
    stringList
  );

let stringList = ["a", "b", "c", "d"];

stringList |> concatList |> Js.log;

let sumList = intList =>
  List.fold_left((sum, currentNumber) => sum + currentNumber, 0, intList);

let intList = [1, 2, 3, 4, 5];

intList |> sumList |> Js.log;